import config
import anyio

from semaphore import Bot, ChatContext

from bot import tor  # Import bot modules.

bot = Bot(config.PHONE_NUMBER)


@bot.handler("^tor")
async def commandHandler(ctx: ChatContext) -> None:
    try:
        msg = ctx.message.get_body().split(" ")
        await tor.handler(ctx, msg)

    except Exception:  # Ensure that the bot does not crash if an exception is hit.
        await ctx.message.reply(f"An error has occured. Please try again.")


async def main():
    async with bot:
        await bot.set_profile(config.BOT_NAME)

        await bot.start()


if __name__ == "__main__":
    anyio.run(main)
