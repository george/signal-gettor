strings = {
    "HIDDEN_MESSAGES_ENABLED": {
        "en-US": """Hey,
It appears that this is the first time you've contacted me! (or, you have set the timer to 0).
        
I have gone ahead and set the chat timer to one hour, which should be plenty of time for you to get a copy of Tor!
If you attempt to reset the timer to not disappear, I will set it back to one hour.

Please delete your original message by the way!"""
    },
    "HELP_MESSAGE": {
        "en-US": """Hi, welcome to the downloader for Tor.

To get a release, please give me a valid command.

valid commands are:
  - tor help [language] (note: tor h [language] will also work)
  - tor verify [language] (note: tor v [language] will also work)
  - tor get <platform> [language] (note: tor g <platform> [language] will also work)
  - tor list platforms/languages (note: tor l p/l will also work)
"""
    },
    "PLATFORM_LIST": {
        "en-US": """{invalid_platform_message}Valid platforms are:
{platform_list}

Note, win means Windows, osx means MacOS. The difference between 32/64 is the "bits" (32-bit, 64-bit)."""
    },
    "INVALID_PLATFORM": {
        "en-US": """The platform you provided, {platform}, is not valid. """
    },
    "LANGUAGE_LIST": {
        "en-US": """{invalid_language_message}Valid languages are:
{language_list}"""
    },
    "INVALID_LANGUAGE": {
        "en-US": """The language you provided, {language}, is not valid. """
    },
    "RELEASE_NOT_ON_DISK": {
        "en-US": """Hey, so, this is awkward.

I actually don't have the release saved right now, but don't fret! I'm grabbing it as we speak, usually, this can take up to two minutes!"""
    },
    "BINARY_DELIVERY_MESSAGE": {
        "en-US": """Your download is ready!
        
Attached, you will find a zip archive of the browser, as well as its corresponding signature file. Extract it, verify it (`tor verify`!), install and enjoy!"""
    },
    "SIGNATURE_VERIFICATION_HELP_MESSAGE": {
        "en-US": """You can find instructions on how to verify the signature of the extracted file over at https://support.torproject.org/tbb/how-to-verify-signature/ (Archived at http://web.archive.org/web/20210308154340/https://support.torproject.org/tbb/how-to-verify-signature/)
        
If you trust me enough, here's a condensed version of the instructions!
Firstly, you must have GnuPG installed.
        
Proceed to fetching the signing key of the Tor development team, which is 0xEF6E286DDA85EA2A4BA7DE684E2C6E8793298290, using the following command:
`gpg --auto-key-locate nodefault,wkd --locate-keys torbrowser@torproject.org`.
        
Export it into a keyring file by using this command: `gpg --output ./tor.keyring --export 0xEF6E286DDA85EA2A4BA7DE684E2C6E8793298290`.
        
Now, verify the signature of the file by running `gpgv --keyring tor.keyring <location_of_this_file> <location_of_extracted_file>`. It should say something along the lines of
"gpgv: Good signature from "Tor Browser Developers (signing key) <torbrowser@torproject.org>""."""
    },
}