import config
import os, uuid, zipfile
import aiohttp, aiofiles

from anyio import run_sync_in_worker_thread
from bot import l10n

# TODO: Documentation?


async def handler(ctx, args):
    if (
        ctx.message.data_message.expires_in_seconds != config.TIMER_SECONDS
        and ctx.message.data_message.groupV2 is None
    ):
        # This ensures that if the timer is not active, it'll be set to an hour.
        #  note: this does NOT affect the timer of groups, as they may be used for other things, and not just Tor delivery.

        await ctx.bot._socket.send(
            {
                "username": config.PHONE_NUMBER,
                "type": "set_expiration",
                "recipientAddress": {"uuid": ctx.message.source.uuid},
                "expiresInSeconds": config.TIMER_SECONDS,
            }
        )

        await ctx.message.reply(
            l10n.strings["HIDDEN_MESSAGES_ENABLED"].get(config.DEFAULT_LANGUAGE)
        )

    arg_len = len(args)

    if arg_len == 2:
        if args[1].lower() == "verify" or args[1].lower() == "v":
            return await verification_help(ctx)
        else:
            return await welcome_message(ctx)

    if arg_len == 3:
        if args[1].lower() == "help" or args[1].lower() == "h":
            if args[2] in config.VALID_LANGUAGES:
                return await welcome_message(ctx, args[2])
            else:
                return await invalid_language(ctx, args[2])

        elif args[1].lower() == "get" or args[1].lower() == "g":
            platform = args[2].lower()

            if platform in config.VALID_PLATFORMS:
                return await deliver(ctx, platform=platform)
            else:
                return await invalid_platform(ctx, platform)

        elif args[1].lower() == "verify" or args[1].lower() == "v":
            if args[2] in config.VALID_LANGUAGES:
                return await verification_help(ctx, args[2])
            else:
                return await invalid_language(ctx, args[2])

        elif args[1].lower() == "list" or args[1].lower() == "l":
            if args[2].lower() == "platforms" or args[2].lower() == "p":
                return await invalid_platform(ctx)

            elif args[2].lower() == "langs" or args[2].lower() == "l":
                return await invalid_language(ctx)

    elif arg_len == 4:
        if args[1].lower() == "get" or args[1].lower() == "g":
            platform = args[2].lower()
            language = args[3]

            if platform in config.VALID_PLATFORMS:
                if language in config.VALID_LANGUAGES:
                    return await deliver(ctx, platform=platform, language=language)
                else:
                    return await invalid_language(ctx, language)
            else:
                return await invalid_platform(ctx, platform)
    else:
        return await welcome_message(ctx)


async def welcome_message(ctx, language=config.DEFAULT_LANGUAGE) -> None:
    """This is the general welcome/help message that will be shown if a command is not recognised."""
    await ctx.message.reply(
        body=l10n.strings["HELP_MESSAGE"].get(
            language, l10n.strings["HELP_MESSAGE"].get(config.DEFAULT_LANGUAGE)
        )
    )


async def verification_help(ctx, language=config.DEFAULT_LANGUAGE) -> None:
    """Provide the user with help with regards to verifying the signature with GPG."""
    await ctx.message.reply(
        body=l10n.strings["SIGNATURE_VERIFICATION_HELP_MESSAGE"].get(
            language,
            l10n.strings["SIGNATURE_VERIFICATION_HELP_MESSAGE"].get(
                config.DEFAULT_LANGUAGE
            ),
        )
    )


async def invalid_platform(
    ctx, platform=None, language=config.DEFAULT_LANGUAGE
) -> None:
    """This will list the available platforms. If a platform is given, it will also tell the user that the platform they provided is invalid."""
    newline = "\n"  # Dumb hack to make python not complain for adding a backslash into a formatted string
    platforms = [f"  - {platform}" for platform in config.VALID_PLATFORMS]

    await ctx.message.reply(
        body=l10n.strings["PLATFORM_LIST"]
        .get(language, l10n.strings["PLATFORM_LIST"].get(config.DEFAULT_LANGUAGE))
        .format(
            invalid_platform_message=f"{l10n.strings['INVALID_PLATFORM'].get(language, l10n.strings['INVALID_PLATFORM'].get(config.DEFAULT_LANGUAGE)).format(platform=platform) if platform is not None else ''}",
            platform_list=newline.join(platforms),
        ),
    )


async def invalid_language(ctx, language=config.DEFAULT_LANGUAGE) -> None:
    """This will list the available languages. If a language is given, it will also tell the user that the language they provided is invalid."""
    newline = "\n"  # Dumb hack to make python not complain for adding a backslash into a formatted string
    langs = [f"  - {lang}" for lang in config.VALID_LANGUAGES]

    await ctx.message.reply(
        body=l10n.strings["LANGUAGE_LIST"]
        .get(language, l10n.strings["LANGUAGE_LIST"].get(config.DEFAULT_LANGUAGE))
        .format(
            invalid_language_message=f"{l10n.strings['INVALID_LANGUAGE'].get(language, l10n.strings['INVALID_LANGUAGE'].get(config.DEFAULT_LANGUAGE)).format(language=language) if language != config.DEFAULT_LANGUAGE else ''}",
            language_list=newline.join(langs),
        ),
    )


async def deliver(ctx, platform, language=config.DEFAULT_LANGUAGE) -> None:
    """Respond with a Tor zip archive, as well as a signature for the requested build.
    If it is not currently present on disk, it will be downloaded"""
    dist_file_path = await construct_file_name(platform, language)

    # Give the user acknowledgement that their message was read.
    #   If a user has read-receipts off, it can be hard to tell if
    #   the bot is working or not.

    await ctx.message.reply(body=config.ACK_EMOJI, reaction=True)

    if not await release_exists(dist_file_path):
        await ctx.message.reply(
            body=l10n.strings["RELEASE_NOT_ON_DISK"].get(
                language,
                l10n.strings["RELEASE_NOT_ON_DISK"].get(config.DEFAULT_LANGUAGE),
            )
        )

        dist_file_path = await download_release(platform, language)

    await ctx.message.reply(
        body=l10n.strings["BINARY_DELIVERY_MESSAGE"].get(
            language,
            l10n.strings["BINARY_DELIVERY_MESSAGE"].get(config.DEFAULT_LANGUAGE),
        ),
        attachments=[{"filename": dist_file_path}],
        quote=True,
    )


# Assistive functions


async def release_exists(file_path) -> bool:
    """Check if a release with this file path exists."""
    return os.path.isfile(file_path)


async def download_release(platform, language) -> str:
    """Download, and temporarily save a release"""

    # TODO: Implement Android build downloader. Seems to be separate from this.
    async with aiohttp.ClientSession() as session:
        async with session.get(config.TOR_DOWNLOAD_ENDPOINT) as resp:
            versions = (await resp.json())["downloads"]

            tor_bin_url = versions[platform][language]["binary"]
            tor_sig_url = versions[platform][language]["sig"]

            base_path = f"{config.TOR_DELIVERY_BASE_PATH}/{platform}/{language}/"
            zip_path = (
                f"{config.TOR_DELIVERY_BASE_PATH}/{platform}/{language}/{platform}.zip"
            )

            bin_tmp_path = f"/tmp/{uuid.uuid4()}"
            sig_tmp_path = f"/tmp/{uuid.uuid4()}"

            async with session.get(tor_bin_url) as resp:
                if resp.status == 200:
                    if not os.path.exists(base_path):
                        await run_sync_in_worker_thread(
                            os.makedirs, base_path, 0o777, True
                        )

                    bin_file = await aiofiles.open(bin_tmp_path, mode="wb")
                    await bin_file.write(await resp.read())
                    await bin_file.close()

            async with session.get(tor_sig_url) as resp:
                if resp.status == 200:
                    sig_file = await aiofiles.open(sig_tmp_path, mode="wb")
                    await sig_file.write(await resp.read())
                    await sig_file.close()

            await run_sync_in_worker_thread(
                zip_file,
                zip_path,
                bin_tmp_path,
                # Original filename of the Tor Browser binary.
                (tor_bin_url.split("/")[-1]),
                sig_tmp_path,
                # Original filename of the Tor Browser binary signature.
                (tor_sig_url.split("/")[-1]),
            )
            return zip_path


def zip_file(
    zip_path, bin_tmp_path, bin_orig_name, sig_tmp_path, sig_orig_name
) -> None:
    """Creates a ZIP file for a release. Used to bypass Signal's "dangerous filetypes" protection measure."""
    zippy = zipfile.ZipFile(zip_path, "x")
    zippy.write(bin_tmp_path, bin_orig_name)
    zippy.write(sig_tmp_path, sig_orig_name)
    zippy.close()

    os.remove(bin_tmp_path)
    os.remove(sig_tmp_path)


async def clean_up_releases() -> None:
    """This removes releases from disk. To be run every 24-48 hours.
    If a release is requested after erasure, it'll be redownloaded.

    This ensures that releases are up-to-date with 24-48 hour lag.
    """

    # TODO: Literally this entire function.
    pass


async def construct_file_name(platform, language) -> str:
    """Constructs the file names for the zip archive and the signature file"""
    return f"{config.TOR_DELIVERY_BASE_PATH}/{platform}/{language}/{platform}.zip"