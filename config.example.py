from config import ACK_EMOJI


BOT_NAME = "Gettor Signal Edition"
PHONE_NUMBER = ""

DEFAULT_LANGUAGE = "en-US"
ACK_EMOJI = "🚀"

TOR_DELIVERY_BASE_PATH = "/usr/app/deliver"

# TODO: Ask in OFTC's Tor channels regarding a better way to programmatically receive a JSON list of builds.
#       at the time of writing, `release/` serves 10.0.13 (only for linux), while `release.old` serves
#       10.0.12 for Windows, Linux, and Mac.
TOR_DOWNLOAD_ENDPOINT = (
    "https://aus1.torproject.org/torbrowser/update_3/release.old/downloads.json"
)

TIMER_SECONDS = 3600

VALID_PLATFORMS = ["win32", "win64", "osx64", "linux32", "linux64", "android"]
VALID_LANGUAGES = [
    "en-US",
    "es-ES",
    "pt-BR",
    "ar",
    "ca",
    "cs",
    "da",
    "de",
    "el",
    "es-AR",
    "fa",
    "fr",
    "ga-IE",
    "he",
    "hu",
    "id",
    "is",
    "it",
    "ja",
    "ka",
    "ko",
    "nb-NO",
    "nl",
    "pl",
    "pt-BR",
    "ru",
    "sv-SE",
    "tr",
    "vi",
    "zh-CN",
    "zh-TW",
]